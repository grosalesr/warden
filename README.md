# Warden

`rsync` wrapper that performs a backup or restore by reading a yaml file, aka backup definition.

Of course `rsync` **is required** and these are the common arguments used to perform operations:

* `verbose`: increase verbosity.
* `progress`: show progress during transfer.
* `human-readable`: output numbers in human-readable format.
* `archive`:  use recursion and preserves all attributes.
    * `rsync` compares permission & attributes at source & target to determine if they are the same file.
    * Specifically `archive` is `-rlptgoD (no -A,-X,-U,-N,-H)`, for details see `rsync` manual.

## Backup definition file

> The backup definition file is read **top to bottom**.

> The backup definition file is also used to restore backups, see considerations section.

The backup definition file is a **[YAML](https://yaml.org/)** file that uses the following configuration block to define a backup target:

```yaml
target_description:     # (string), mandatory, must be unique
    src:                # (string), mandatory, directory where to look for files or dirs
    dst:                # (string), mandatory, directory where put the files or dirs
    backup:             # (list), mandatory, Files or directories to backup
    delete:             # (bool), files removed in source will be removed after transfer, not during
    exclude:            # (list) exclude files matching PATTERN
```

Multiple backup targets can be defined using this block.

Check out the 'backup_definitions.yml' example file.

### Considerations

#### User `$HOME` directory

The special keyword `HOME` can be used in **src** & **dst** keys as reference for the user home directory.

As an example, if the user **user**, defines the following:

```yaml
src: HOME
dst: HOME/Documents/dots
```

`warden` will interpret it as:

* `HOME` -> `/home/user`
* `HOME/Documents/dots` -> `/home/user/Documents/dots`

#### Defining remote targets

> remote backups using `ssh` is not supported.

> A remote target, is a *remote* filesystem that is mounted locally, eg: NFS.

The special keyword **remote** can be used in the 'target_description', eg: `remote_backup`.

This will ensure the following behavior:

* When performing a `backup`, `remote_backup` will be executed **last**.
* When performing a `restore`, `remote_backup` will be executed **first**.

#### dot files/dirs

* When performing a `backup`, the leading dot, `.`, is removed, eg: `.myfile -> myfile`
* When performing a `restore`, the leading dot, `.`, is restored, eg: `myfile -> .myfile1`

#### directories

In order to backup entire directories and for human readability, add a *trailing forward slash*, `/`, is **required**. eg:

```yaml
backup:
    - mydir1/
    - myfile
```

# Usage
## Sub Commands

* **version**: Shows application version and build.
* **backup**: Reads backup definition file and performs the backup.
* **restore**: Reads backup definition file and performs the restore.
    * Inverts source & destination from the backup definition file

### Sub Commands Flags
**backup** & **restore**
* `-f`, `--file`: Backup targets file (YAML)
* `-q`, `--quiet`: Suppress all stdout.
* `-c`, `--show-commands`: Shows the `rsync` commands to be executed

### Global Flags

* `-h`, `--help`: Shows help. Works for **backup** & **restore** subcommands.

package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"os/exec"
	"regexp"
	"strings"

	"gopkg.in/yaml.v3"
)

func parse(backupDefinition string, logger *log.Logger) {
	// Opens the backup definition file and tries to unmarshal it
	// the backup definition file must be YAML.
	data, err := os.ReadFile(backupDefinition)
	if err != nil {
		logger.SetPrefix("ERROR ")
		logger.Fatal("Can't open the backup definition file")
	} else {
		logger.Println("backup definition file:", backupDefinition)
	}

	err = yaml.Unmarshal([]byte(data), &targets)
	if err != nil {
		logger.SetPrefix("ERROR ")
		logger.Fatal("Can't parse the backup definition file")
	}
}

func checkHomeEnvKey(str string) string {
	// if string contains HOME keyword, it is expanded to /home/user/
	// where 'user' is the user running the application
	sep := "/"
	homeEnvKey := "HOME"

	if strings.Contains(str, homeEnvKey) {
		homeAbsolutePath := os.Getenv(homeEnvKey)

		splitPath := strings.Split(str, sep)
		splitPath[0] = homeAbsolutePath
		str = strings.Join(splitPath, sep)
	}

	if !strings.HasSuffix(str, sep) {
		str += sep
	}

	return str
}

func checkSpecialArgs(key string) string {
	// rsync optional flags
	specialArgs := ""
	exclude := "--exclude"
	deleteAfter := "--delete-after"

	if targets[key].Del {
		specialArgs += deleteAfter + " "
	}

	if targets[key].Exclusions != nil {
		for i := range targets[key].Exclusions {
			specialArgs += exclude + " " + targets[key].Exclusions[i] + " "
		}
	}

	return specialArgs
}

func maskPath(path string) string {
	// if space is found in path, it is replaced to pattern.

	pattern := "___"
	whitespace := regexp.MustCompile(`\s`).MatchString(path)

	if whitespace {
		path = strings.ReplaceAll(path, " ", pattern)
	}

	return path
}

func restorePath(path string) (bool, string) {
	// if pattern if found in path, it is replaced to space.
	pattern := "___"
	replacedSpace := regexp.MustCompile(pattern).MatchString(path)

	if replacedSpace {
		path = strings.ReplaceAll(path, pattern, " ")
	}

	return replacedSpace, path
}

func showRsyncCmd(rsyncBase []string, rsyncArgs []string) {
	for _, rsyncArg := range rsyncArgs {

		tmp := strings.Fields(rsyncArg)
		for i, val := range tmp {
			changed, path := restorePath(val)
			if changed {
				tmp[i] = "'" + path + "'"
			}
		}

		rsyncCmd := strings.Join(append(rsyncBase, tmp...), " ")
		fmt.Println(rsyncCmd)
	}
}

func runCmd(rsyncBase []string, rsyncArgs []string, runMode string, logger *log.Logger) {
	logger.Printf("starting %s targets.\n", runMode)

	for _, rsyncArg := range rsyncArgs {
		rsyncCmd := append(rsyncBase, strings.Fields(rsyncArg)...)

		for i, val := range rsyncCmd {
			changed, path := restorePath(val)
			if changed {
				rsyncCmd[i] = path
			}
		}
		//fmt.Printf("%+q\n", rsyncCmd) // debug

		logger.Println(strings.Join(rsyncCmd, " "))
		out, err := exec.Command(rsyncCmd[0], rsyncCmd[1:]...).Output()
		if err != nil {
			var pErr *exec.ExitError

			if errors.As(err, &pErr) {
				logger.SetPrefix("ERROR ")

				errStr := getErrorStr(pErr.ExitCode())
				logger.Fatalf("rsync %s: %s", err, errStr)
			}
		}
		logger.Printf("%s\n", out)
	}
	logger.Printf("%s targets processed successfully.\n", runMode)
}

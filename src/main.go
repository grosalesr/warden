package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
)

var (
	version string
	build   string
	quiet   bool

	logger *log.Logger

	targets = make(map[string]Target)
)

const logFile = "/tmp/warden.log"
const wardenUsage = `Usage: warden [global options] [command] [options]

Available commands:
  backup    Backup files as defined in the 'backup definition' file

  restore   Restores files from backup.
            this means, invert source & destination.

  version   Shows application version and build

Global options:
  --quiet, -q   Suppress all standard output.
`

func showVersion() {
	fmt.Printf("warden version: %s Build: %s\n", version, build)
	os.Exit(0)
}

func main() {
	flag.BoolVar(&quiet, "q", false, "Suppress all standard output.")
	flag.BoolVar(&quiet, "quiet", false, "Suppress all standard output.")

	flag.Usage = func() {
		fmt.Printf(wardenUsage)
	}

	flag.Parse()
	args := flag.Args()

	if len(args) == 0 {
		fmt.Println("No command given. See 'warden --help'.")
		os.Exit(1)
	}

	file, err := os.OpenFile(logFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error creating logfile %s: %v", logFile, err)
		log.Fatal(err)
	}
	defer file.Close()

	if quiet {
		logger = log.New(file, "INFO ", log.Ldate|log.Ltime|log.Lmsgprefix)
	} else {
		logger = log.New(io.MultiWriter(file, os.Stdout), "INFO ", log.Ldate|log.Ltime|log.Lmsgprefix)
	}

	switch runMode := args[0]; runMode {
	case "version":
		showVersion()

	case "backup", "restore":
		locateRsyncPath(logger)
		subCmd(runMode, args[1:], logger)

	default:
		err := fmt.Sprintf("Command '%v' does not exist. See 'warden --help'.\n", runMode)
		fmt.Printf(err)
		os.Exit(1)
	}
}

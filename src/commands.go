package main

import (
	"flag"
	"fmt"
	"log"
	"os"
)

const backupUsage = `Usage: warden [backup|restore] [options]

Available options:
  --help, -h            shows usage

  --file, -f            backup definition YAML file.
                        For dot files, dot is removed.

  --show-commands, -c   Parses backup definition file and shows rsync commands
                        that will be executed.
                        A backup definition file must be provided
`

func subCmd(runMode string, args []string, logger *log.Logger) {
	var showCmd bool
	var rsyncBase []string
	var rsyncArgs []string
	var backupDefinition = ""

	fs := flag.NewFlagSet("backup", flag.ExitOnError)

	fs.BoolVar(&showCmd, "c", false, "Shows rsync commands from backup definition file")
	fs.BoolVar(&showCmd, "show-commands", false, "Shows rsync commands from backup definition file")

	fs.StringVar(&backupDefinition, "f", "", "path to backups definition YAML file")
	fs.StringVar(&backupDefinition, "file", "", "path to backups definition YAML file")

	fs.Usage = func() {
		fmt.Printf(backupUsage)
	}

	fs.Parse(args)

	if len(args) == 0 {
		fmt.Printf("No options given. See 'warden %s --help'.\n", runMode)
		os.Exit(1)
	}

	rsyncBase = append(rsyncBase, rsync.cmd, rsync.verbose, rsync.archive,
		rsync.progress, rsync.readability)

	parse(backupDefinition, logger)

	switch runMode {
	case "backup":
		rsyncArgs = makeBackupArgs(rsyncArgs)

	case "restore":
		rsyncArgs = makeRestoreArgs(rsyncArgs)
	}

	if showCmd {
		showRsyncCmd(rsyncBase, rsyncArgs)
		os.Exit(0)
	}

	runCmd(rsyncBase, rsyncArgs, runMode, logger)
}

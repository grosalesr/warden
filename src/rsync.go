package main

import (
	"log"
	"os/exec"
	"slices"
	"strings"
)

// common rsync parameters to be used
var rsync = struct {
	cmd         string
	archive     string
	verbose     string
	progress    string
	readability string
}{
	cmd:         "rsync",
	archive:     "--archive",
	verbose:     "--verbose",
	progress:    "--progress",
	readability: "--human-readable",
}

const (
	dir_suffix    = "/"
	hidden_prefix = "."
	remoteTargets = "remote" // especial keyword to identify remote targets
)

// from rsync manual - EXIT VALUES section, not all exit values are here
var exitValues = map[int]string{
	2:  "Protocol incompatibility",
	3:  "Errors selecting input/output files, dirs",
	10: "Error in socket I/O",
	11: "Error in file I/O",
	23: "Partial transfer due to error",
	24: "Partial transfer due to vanished source files",
	30: "Timeout in data send/receive",
}

func locateRsyncPath(logger *log.Logger) {
	// uses initial definition of rsync.cmd to lookup executable's full path
	path, err := exec.LookPath(rsync.cmd)
	if err != nil {
		logger.SetPrefix("ERROR ")
		logger.Fatal(err)
	}

	rsync.cmd = path
}

func getErrorStr(exitCode int) string {
	exitCodeNotFound := "Check rsync manual for details"

	if strErr, ok := exitValues[exitCode]; ok {
		return strErr
	}

	return exitCodeNotFound
}

/*
   rsync syntax:
   * To copy entire directories, do not use trailing forward slash
   * To copy directory content, DO use trailing forward slash

   the desired behavior for makeBackupArgs & makeRestoreArgs is to
   copy directory contents hidden directories must retain the syntax
   as defined in order to copy contents from .myDir to myDir
*/

/* TO DO:
maps, which provides maps.Keys(), package still on experimental
see:
  https://github.com/golang/go/issues/61900
  https://github.com/golang/go/issues/68261
*/

func makeBackupArgs(rsyncArgs []string) []string {
	var keys []string
	var remote_keys []string

	for k := range targets {
		if strings.Contains(k, remoteTargets) {
			remote_keys = append(remote_keys, k)
		} else {
			keys = append(keys, k)
		}
	}
	keys = append(keys, remote_keys...)

	for k := range keys {
		rsyncTargetArgs := checkSpecialArgs(keys[k])

		targetSrc := checkHomeEnvKey(targets[keys[k]].Src)
		targetDst := checkHomeEnvKey(targets[keys[k]].Dst)

		var nonDotTargets string
		for kk := range targets[keys[k]].Backup {
			targetName := targets[keys[k]].Backup[kk]

			if strings.HasPrefix(targetName, hidden_prefix) {
				newTargetName := strings.TrimPrefix(targetName, hidden_prefix)

				dotSrc := maskPath(targetSrc + targetName)
				dotTarget := rsyncTargetArgs + dotSrc + " " + targetDst + newTargetName

				rsyncArgs = append(rsyncArgs, dotTarget)

			} else {
				trimmedPath := strings.TrimSuffix(targetName, dir_suffix)
				curatedPath := maskPath(targetSrc + trimmedPath)
				nonDotTargets += curatedPath + " "
			}
		}

		if len(nonDotTargets) > 0 {
			rsyncTargetArgs += nonDotTargets + targetDst
			rsyncArgs = append(rsyncArgs, rsyncTargetArgs)
		}
	}

	return rsyncArgs
}

func makeRestoreArgs(rsyncArgs []string) []string {
	var keys []string

	// run remote targets first
	for k := range targets {
		if strings.Contains(k, remoteTargets) {
			keys = slices.Insert(keys, 0, k)
		} else {
			keys = append(keys, k)
		}
	}

	for k := range keys {
		targetSrc := checkHomeEnvKey(targets[keys[k]].Dst)
		targetDst := checkHomeEnvKey(targets[keys[k]].Src)

		var nonDotTargets string
		for kk := range targets[keys[k]].Backup {
			targetName := targets[keys[k]].Backup[kk]

			if strings.HasPrefix(targetName, hidden_prefix) {
				sourceName := strings.TrimPrefix(targetName, hidden_prefix)
				dotTarget := targetSrc + sourceName + " " + targetDst + targetName

				rsyncArgs = append(rsyncArgs, dotTarget)

			} else {
				nonDotTargets += targetSrc + strings.TrimSuffix(targetName, dir_suffix) + " "
			}
		}

		if len(nonDotTargets) > 0 {
			nonDotTargets += targetDst
			rsyncArgs = append(rsyncArgs, nonDotTargets)
		}
	}

	return rsyncArgs
}

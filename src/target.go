package main

// YAML content: backup targets definition
type Target struct {
	Src        string   `yaml:"src"`
	Dst        string   `yaml:"dst"`
	Del        bool     `yaml:"delete"`
	Backup     []string `yaml:"backup"`
	Exclusions []string `yaml:"exclude"`
}

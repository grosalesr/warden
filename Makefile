.SILENT:

# set to true for dev builds
BINARY = warden
BUILD = $(shell date +"%Y%m%d.%H%M%S")

# Make parses the $$ and invokes awk with a single $
NEXT = $(shell awk '$$1 == "next:" {print $$2}' version.txt)
VERSION = $(shell awk '$$1 == "current:" {print $$2}' version.txt)

# test if NEXT has a non-empty value
ifneq ($(strip $(NEXT)),)
	override BINARY := $(BINARY)_next
	override VERSION := $(NEXT)-next
endif

clean:
	rm -rf bin/*

build:

	$(info building $(BINARY) version: $(VERSION))

	go build \
		-ldflags "-X main.version=${VERSION} -X main.build=${BUILD}" \
		-o bin/${BINARY} \
		src/*.go

run:
	go run main.go

version:
	./bin/${BINARY} version

test-backup: version
	./bin/${BINARY} backup --show-commands --file backup_definitions.yml

test-restore: version
	./bin/${BINARY} restore --show-commands --file backup_definitions.yml
